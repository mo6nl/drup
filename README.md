# Drup

This tool will query a website to find out if it is running Drupal and if so: determine the version of Drupal used.
This is done by loading some publically available Drupal files (js files) which are specific for Drupal and for a major Drupal version used.
A hash of the files loaded is compared to a table with known versions to determine which version of Drupal is installed.
This is a sufficient reliable way to see which Drupal version a site is running.

## Usage

- Clone repo
- `./drup.sh <site-url>` where site-url is in the format: http://www.example.com or https://www.example.com

## Compatibity

Currently Drupal 5, 6 and 7 hashes are included. Work to expand to Drupal 8 is in progress.

## References

This tool is based on an idea of Bert Boerland as described in: http://willy.boerland.com/myblog/fingerprinting_a_drupal_iste_what_version_is_that_site_running

## See also

- Droopescan: A plugin-based scanner that aids security researchers in identifying issues with several CMSs, mainly Drupal & Silverstripe. - https://github.com/droope/droopescan

## Author(s)

- George Moses mailto:george.moses@wunderkraut.com
