<?php

/**
 * @file
 * Determine Drupal version.
 */

define('GETCHANGELOG',FALSE);
define('GETMD5',TRUE);
define('PAGEWAIT',1);

/**
 * @param $jsfileversions
 * @param $drupalversion
 * @return array
 */
function drupalversions($jsfileversions, $drupalversion) {
  require_once('drupal' . $drupalversion . '-md5.inc');
  $versions = array();

  foreach ($jsmd5s as $dversion => $jsmd5) {
    /*
    print $dversion . PHP_EOL;
    print_r($jsmd5);
    print_r($jsfileversions);
    */
    $diff = array_diff_assoc($jsmd5, $jsfileversions);
    $found = (count($diff) == 0);
    if ($found) {
      $versions[] = $dversion;
    }
  }

  return $versions;
}

/**
 * @param $url
 */
function deter($url) {
  $drupaljsmd5s = array(
    'afd188dc6cd982d37463209679ab01ec' => 5,
    '2c5e4277fec6afac333e913744e0408f' => 5,
    '4677b027fed107133090dabccee2b4f5' => 5,
    '9e557006e956d365119eb2ebd2169051' => 5,

    "ebbcc0156242a08a25c596432ca92f67" => 6,
    "2ff7dc985e57d1139ce4dc844b06bc64" => 6,
    "398b3832c2de0a0ebd08cb7f2afe1545" => 6,
    "88682723723be277fb57c0d8e341c0cf" => 6,
    "9a1c645566d780facee5ce1a0d3fab7c" => 6,
    "fe6f8c678cb511d68a3dbe5a94f2e278" => 6,
    "90c0aa7ed8581884c2afe73fc87b5697" => 6,
    "1904f6fd4a4fe747d6b53ca9fd81f848" => 6,

    '847afc6e14d280e66a564194e166a66e' => 7,
    'f3f32021901f4c33d2eebbc634de587d' => 7,
    'cbd95e04bad1be13320ebbe63a863245' => 7,
    'd4515f21acd461ca04304233bf0daa1e' => 7,
    'f9281a1fa8b926ba038bebc3bb0c3d61' => 7,
    '0bb055ea361b208072be45e8e004117b' => 7,
    'cea76de12c5bb95dd0789fbd3cd754f0' => 7,
    'bb9f18fb7a42b95fcad87b98dbb19c87' => 7,

    '6c0e67075b302cfd61c1e0abfea1de6c' => 8,
    '223beb084ce1be6fbe6da639775597e0' => 8,
  );

  $js5files = array(
    'autocomplete.js',
    'collapse.js',
    'drupal.js',
    'jquery.js',
    'progress.js',
    'tableselect.js',
    'textarea.js',
    'update.js',
    'upload.js',
  );

  $js6files = array(
    'ahah.js',
    'autocomplete.js',
    'batch.js',
    'collapse.js',
    'drupal.js',
    'form.js',
    'jquery.form.js',
    'jquery.js',
    'progress.js',
    'tabledrag.js',
    'tableheader.js',
    'tableselect.js',
    'teaser.js',
    'textarea.js',
  );

  $js7files = array(
    'ajax.js',
    'authorize.js',
    'autocomplete.js',
    'batch.js',
    'collapse.js',
    'drupal.js',
    'form.js',
    'jquery.ba-bbq.js',
    'jquery.cookie.js',
    'jquery.form.js',
    'jquery.js',
    'jquery.once.js',
    'machine-name.js',
    'progress.js',
    'states.js',
    'tabledrag.js',
    'tableheader.js',
    'tableselect.js',
    'textarea.js',
    'timezone.js',
    'vertical-tabs.js',
  );

  // Add http if nessecary
  if (substr($url,0,7) != 'http://') {
    if (substr($url,0,8) != 'https://') {
      $url = 'http://' . $url;
    }
  }

  $parts = parse_url($url);
  $host = $parts['host'];
  $ip = gethostbyname($host);
  if (!$ip) {
    return;
  }
  if ($ip == $host) {
    return;
  }
  $info = array();

  // URL eindigen met /
  if ($url[strlen($url)-1] != '/') {
    $url .= '/';
  }

  // Basic info
  print 'Get basic IP info...';
  $info['URL'] = $url;
  $info['IP'] = $ip;
  $info['Host IP'] = gethostbyaddr($ip);
  print PHP_EOL;

  // Whois IP
  print 'Get WHOIS description info...';
  $whois = array();
  exec('whois ' . $ip . ' | grep descr', $whois);
  if (count($whois) > 0) {
    $info['Whois descr'] = trim(str_replace('descr:','',$whois[0]));
  }
  print PHP_EOL;

  // Whois IP
  print 'Get WHOIS Organization info...';
  $whois = array();
  exec('whois ' . $ip . ' | grep Organization', $whois);
  if (count($whois) > 0) {
    $info['Whois org'] = trim(str_replace('Organization:','',$whois[0]));
  }
  print PHP_EOL;

  // Ping server
  print "Ping server 3 times...";
  exec('ping -c 3 ' . $ip . ' | grep round-trip', $ping);
  if (count($ping) > 0) {
    $info['Ping'] = $ping[0];
  }
  print PHP_EOL;

  $isdrupal = FALSE;

  $opts = array(
    'http' => array(
      'method' => "GET",
      'header' => "Accept-language: nl,en\r\n",
      'user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:17.0) Gecko/20100101 Firefox/17.0',
    ),
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
    ),
  );

  // Set user agent
  $context = stream_context_create($opts);
  libxml_set_streams_context($context);

  print "Loading homepage: $url ";
  $dom = new DOMDocument('1.0');
  $starttime = microtime(TRUE);
  $html = @file_get_contents($url, false, $context);
  $endtime = microtime(TRUE);
  if ($html == '') {
    echo "Error loading: " . $url . PHP_EOL;
    exit;
  }
  print PHP_EOL;
  $info['Duration'] = number_format($endtime - $starttime,2,'.','') . ' sec.';

  print "Parsing HTML...";
  @$dom->loadHTML($html);

  // Look for Generator meta tag
  $anchors = $dom->getElementsByTagName('meta');
  foreach ($anchors as $element) {
    $attr = $element->getAttribute('name');
    if ($attr == 'Generator') {
      $generator = $element->getAttribute('content');
      $info['Generator'] = $generator;
      if (substr($generator,0,6) == 'Drupal') {
        $info['Major'] = substr($generator,7,1);
        $isdrupal = TRUE;
      }
    }
  }

  // Look for mailto: hrefs
  $anchors = $dom->getElementsByTagName('a');
  foreach ($anchors as $element) {
    $attr = $element->getAttribute('href');
    if (substr($attr,0,6) == 'mailto') {
      $info['Mail'] = str_replace('mailto:','',$attr);
      break;
    }
  }
  print PHP_EOL;

  if (!$isdrupal) {
    // Look for Drupal.settings
    if (strpos($html,'Drupal.settings') !== FALSE) {
      $isdrupal = TRUE;
      $info['Major'] = '6';
      $info['Version'] = '6.x';
    }
  }

  $subdir = '';
  if ($info['Major'] == '8') {
    $subdir = 'core/';
  }

  // Check for drupal.js
  if (GETMD5) {
    sleep(PAGEWAIT);
    print "Fingerprinting drupal.js...";
    $drupaljs = @file_get_contents($url . $subdir . 'misc/drupal.js', false, $context);
    if (strlen($drupaljs) > 0) {
      $isdrupal = TRUE;
      $info['md5']['drupal.js'] = md5($drupaljs);
      if (isset($drupaljsmd5s[$info['md5']['drupal.js']])) {
        $info['Major'] = $drupaljsmd5s[$info['md5']['drupal.js']];
        $info['Version'] = $drupaljsmd5s[$info['md5']['drupal.js']] . '.x';
      }
      else {
        $drupaljs = trim($drupaljs);
        $d7s = "var Drupal = Drupal || { 'settings': {}, 'behaviors': {}, 'locale': {} };";
        $d8s = "/**\n *\n @file * Defines the Drupal JavaScript API.";
        if (substr($drupaljs,0,24) == '// $Id: drupal.js,v 1.29') {
          $info['Major'] = '5';
          $info['Version'] = '5.x';
        }
        elseif (substr($drupaljs,0,24) == '// $Id: drupal.js,v 1.22') {
          $info['Major'] = '4';
          $info['Version'] = '4.x';
        }
        elseif (substr($drupaljs,0,60) == "/**\n * Override jQuery.fn.init to guard against XSS attacks.") {
          $info['Major'] = '6';
          $info['Version'] = '6.x';
        }
        elseif (substr($drupaljs,0,strlen($d7s)) == $d7s) {
          $info['Major'] = '7';
          $info['Version'] = '7.x';
        }
        elseif (substr($drupaljs,0,strlen($d8s)) == $d8s) {
          $info['Major'] = '8';
          $info['Version'] = '8.x';
        }
        else {
          $isdrupal = FALSE;
        }
      }
    }
    print PHP_EOL;

    if ($isdrupal) {
      if (($info['Major'] == '5') || ($info['Major'] == '6') || ($info['Major'] == '7')) {
        if ($info['Major'] == '5') {
          $getfiles = $js5files;
        }
        if ($info['Major'] == '6') {
          $getfiles = $js6files;
        }
        if ($info['Major'] == '7') {
          $getfiles = $js7files;
        }

        print "Reading all Drupal " . $info['Major'] . " /misc/*.js files...";
        foreach ($getfiles as $file) {
          $drupaljs = @file_get_contents($url . $subdir . 'misc/' . $file, false, $context);
          $info['md5'][$file] = md5($drupaljs);
        }
        print PHP_EOL;
        $info['Versions'] = drupalversions($info['md5'],$info['Major']);
      }
    }

  }

  // Get ChangeLog, suppress error
  if (GETCHANGELOG) {
    sleep(PAGEWAIT);
    $changelog = @file_get_contents($url . $subdir . 'CHANGELOG.txt', false, $context);
    $changelog = trim($changelog);
    if (substr($changelog,0,6) == 'Drupal') {
      $isdrupal = TRUE;
      $info['Changelog'] = substr($changelog,0,23);
      if (preg_match('/^Drupal (\d+)\.(\d+)/', $changelog, $matches)) {
        $info['Version'] = $matches[1] . '.' . $matches[2];
        $info['Major'] = $matches[1];
        $info['Minor'] = $matches[2];
      }
    }
  }

  $info['Is Drupal'] = ($isdrupal ? 'Ja' : 'Nee');
  print_r($info);
}

if ($argv[1] != '') {
  deter($argv[1]);
}
